#include "MMInput.h"
#include "MMGame.h"
#include "MMMultiPlayer.h"

#include "MMFighter.h"
#include "MMGameWorld.h"


using namespace MMGame;


MovementAction::MovementAction(ActionType type, unsigned_int32 moveFlag, unsigned_int32 specFlag) : Action(type)
{
	movementFlag = moveFlag;
	spectatorFlag = specFlag;
}

MovementAction::~MovementAction()
{
}

void MovementAction::HandleEngage(void)
{


	const Player *player = TheMessageMgr->GetLocalPlayer();
	if (player)
	{
		const FighterController *controller = static_cast<const GamePlayer *>(player)->GetPlayerController();
		if (controller)
		{
			ClientMovementMessage message(kMessageClientMovementBegin, movementFlag, controller->GetLookAzimuth(), controller->GetLookAltitude());
			TheMessageMgr->SendMessage(kPlayerServer, message);
			return;
		}
	}

	GameWorld *world = static_cast<GameWorld *>(TheWorldMgr->GetWorld());
	if (world)
	{
		SpectatorCamera *camera = world->GetSpectatorCamera();
		camera->SetSpectatorFlags(camera->GetSpectatorFlags() | spectatorFlag);
	}
}

void MovementAction::HandleDisengage(void)
{
	const Player *player = TheMessageMgr->GetLocalPlayer();
	if (player)
	{
		const FighterController *controller = static_cast<const GamePlayer *>(player)->GetPlayerController();
		if (controller)
		{
			ClientMovementMessage message(kMessageClientMovementEnd, movementFlag, controller->GetLookAzimuth(), controller->GetLookAltitude());
			TheMessageMgr->SendMessage(kPlayerServer, message);
			return;
		}
	}

	GameWorld *world = static_cast<GameWorld *>(TheWorldMgr->GetWorld());
	if (world)
	{
		SpectatorCamera *camera = world->GetSpectatorCamera();
		camera->SetSpectatorFlags(camera->GetSpectatorFlags() & ~spectatorFlag);
	}
}

void MovementAction::HandleMove(int32 value)
{
	static const unsigned_int32 movementFlags[8] =
	{
		kMovementForward,
		kMovementForward | kMovementRight,
		kMovementRight,
		kMovementRight | kMovementBackward,
		kMovementBackward,
		kMovementBackward | kMovementLeft,
		kMovementLeft,
		kMovementLeft | kMovementForward
	};

	static const unsigned_int32 spectatorFlags[8] =
	{
		kSpectatorMoveForward,
		kSpectatorMoveForward | kSpectatorMoveRight,
		kSpectatorMoveRight,
		kSpectatorMoveRight | kSpectatorMoveBackward,
		kSpectatorMoveBackward,
		kSpectatorMoveBackward | kSpectatorMoveLeft,
		kSpectatorMoveLeft,
		kSpectatorMoveLeft | kSpectatorMoveForward
	};

	const Player *player = TheMessageMgr->GetLocalPlayer();
	if (player)
	{
		const FighterController *controller = static_cast<const GamePlayer *>(player)->GetPlayerController();
		if (controller)
		{
			unsigned_int32 flags = (value >= 0) ? movementFlags[value] : 0;
			ClientMovementMessage message(kMessageClientMovementChange, flags, controller->GetLookAzimuth(), controller->GetLookAltitude());
			TheMessageMgr->SendMessage(kPlayerServer, message);
			return;
		}
	}
	GameWorld *world = static_cast<GameWorld *>(TheWorldMgr->GetWorld());
	if (world)
	{ 
		unsigned_int32 flags = (value >= 0) ? spectatorFlags[value] : 0; 
		SpectatorCamera *camera = world->GetSpectatorCamera();
		camera->SetSpectatorFlags((camera->GetSpectatorFlags() & ~kSpectatorPlanarMask) | flags); 
	} 
}

FireAction::FireAction(ActionType type) : Action(type)
{
}

FireAction::~FireAction()
{
}

void FireAction::HandleEngage(void)
{
	const Player *player = TheMessageMgr->GetLocalPlayer();
	if (player)
	{
		FighterController *controller = static_cast<const GamePlayer *>(player)->GetPlayerController();
		if (controller)
		{
			// The player is controlling a fighter, so either fire the weapon or interact with a panel effect.

			const FighterInteractor *interactor = controller->GetFighterInteractor();
			const Node *interactionNode = interactor->GetInteractionNode();
			if ((!interactionNode) || (interactionNode->GetNodeType() != kNodeEffect))
			{
				// No panel effect, so fire the weapon.
				//check whether player firing is alive
				if (!(controller->GetFighterFlags() & kFighterDead))
				{
					ClientFiringMessage message((GetActionType() == kActionFirePrimary) ? kMessageClientFiringPrimaryBegin : kMessageClientFiringSecondaryBegin, controller->GetLookAzimuth(), controller->GetLookAltitude());
					TheMessageMgr->SendMessage(kPlayerServer, message);
				}
			}
		}
		else
		{
			if (TheMessageMgr->GetSynchronizedFlag())
			{
				if ((!TheMessageMgr->GetServerFlag()))
				{
					TheMessageMgr->SendMessage(kPlayerServer, ClientMiscMessage(kMessageClientSpawn));
				}
			}
		}
	}
}

MenuAction::MenuAction(ActionType type) : Action(type)
{
}

MenuAction::~MenuAction()
{
}

void MenuAction::HandleEngage(void)
{
	const Player *player = TheMessageMgr->GetLocalPlayer();
	if (player)
	{
		FighterController *controller = static_cast<const GamePlayer *>(player)->GetPlayerController();
		if (controller)
		{
			TheDisplayBoard->ShowPauseMenu();
		}
		
	}
}
