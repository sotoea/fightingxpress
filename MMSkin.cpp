//
//  MMSkin.cpp
//



#include "MMSkin.h"
#include "MMGame.h"
#include "MMGameWorld.h"
#include "MMFighter.h"
#include "TSConfigData.h"
#include "TSPanels.h"
#include <stdio.h>


using namespace MMGame;



DisplayBoard *MMGame::TheDisplayBoard = nullptr;
float splashTimer = 0;

void DisplayBoard::ButtonPress()
{
	GameWorld *world = static_cast<GameWorld *>(TheWorldMgr->GetWorld());
	const Point3D pos = Point3D(0.0F, 0.0F, 2.0F);
	GamePlayer *player = static_cast<GamePlayer *>(TheMessageMgr->GetLocalPlayer());
	world->ReqestOjectAtLocation(pos, kSoldierEntity, player->GetPlayerKey());

	startBut->HideWidget();
	startPic->HideWidget();
	pauseWin->HideWidget();
}



GameWindow::GameWindow(const char *panelName) : Window(panelName)
{
}

GameWindow::~GameWindow()
{
}



DisplayBoard::DisplayBoard() :  Global<DisplayBoard>(TheDisplayBoard)
{
	Integer2D screenSize = TheDisplayMgr->GetDisplaySize();

    myScore = new TextWidget(Vector2D(350.0F, 16.0F), "");
	myScore->SetFont("font/Bold");
	myScore->SetTextScale(3.0f);
	myScore->SetWidgetColor(ColorRGBA(1.0f, 1.0f, 1.0f)); // WHITE
	myScore->SetWidgetPosition(Point3D(50, 50, 0));

	myHealth = new ProgressWidget(Vector2D(300.0f, 20.0F));
	myHealth->SetWidgetColor(ColorRGBA(0.0f, 1.0f, 0.0f),kWidgetColorHilite);
    myHealth->SetWidgetColor(ColorRGBA(0.0f, 0.0f, 0.0f));
	myHealth->SetWidgetPosition(Point3D(10, 20, 0));
	myHealth->SetMaxValue(100);
    myHealth->SetValue(100);
	myHealth->SetWidgetKey("health");
	
	reticle = new ImageWidget(Vector2D(40.0F, 40.0F));
	reticle->SetWidgetPosition(Point3D(TheDisplayMgr.GetDisplaySize().x / 2 - 20, TheDisplayMgr.GetDisplaySize().y / 2 - 20, 0));
	reticle->SetWidgetColor(ColorRGBA(1.0f, 1.0f, 1.0f, 1.0f));
	reticle->SetTexture(0, "Reticle");

	deathCounter = new TextWidget(Vector2D(300.0F, 16.0F), "");
	deathCounter->SetFont("font/Bold");
	deathCounter->SetTextScale(2.0f);
	deathCounter->SetWidgetColor(ColorRGBA(1.0f, 1.0f, 1.0f)); // WHITE
	deathCounter->SetWidgetPosition(Point3D(screenSize[0] / 2 - 100, screenSize[1] / 2 - 5, 0));
	
	endingMessage = new TextWidget(Vector2D(350.0F, 16.0F), "");
	endingMessage->SetFont("font/Bold");
	endingMessage->SetTextScale(2.0f);
	endingMessage->SetWidgetColor(ColorRGBA(1.0f, 1.0f, 1.0f)); // WHITE
	endingMessage->SetWidgetPosition(Point3D(screenSize[0] / 2 - 120, screenSize[1] / 2 + 100, 0));

	// Adds widget to the screen
	TheInterfaceMgr->AddWidget(myScore);
	TheInterfaceMgr->AddWidget(myHealth);
	TheInterfaceMgr->AddWidget(reticle);

	TheInterfaceMgr->AddWidget(deathCounter);
	TheInterfaceMgr->AddWidget(endingMessage);

	UpdateScore(0);

	// THIS IS TO TEST A PANEL YOU NEED A PANEL CALLED START FOR THIS
	pauseWin= new StartWindow("Splash");
	pauseWin->ActivateWidget();
	TheInterfaceMgr->AddWidget(pauseWin);
	//pauseWin->HideWidget();

	UpdateDisplayPosition();
}

DisplayBoard::~DisplayBoard()
{
	delete pauseWin;
	delete myScore;
	delete myHealth;
	delete reticle;
}



void DisplayBoard::OpenBoard(void)
{
    if (!TheDisplayBoard){
		 TheGame->AddBoard(new DisplayBoard);
    }
}

//not sure why this is not called on a mouse event
void DisplayBoard::HandleWidgetEvent(Widget *widget, const WidgetEventData *eventData)
{

	startBut->SetWidgetColor(ColorRGBA(1.0f, 0.0f, 0.0f));
	if (eventData->eventType == kEventMouseDown)
	{

		ButtonPress();
	}
}





void DisplayBoard::Preprocess(void)
{
	GameBoard::PreprocessWidget();
	
}

void DisplayBoard::MoveWidget(void)
{
        Board::MoveWidget();
}

void DisplayBoard::UpdateDisplayPosition(void)
{
	InvalidateWidget();

}

void DisplayBoard::UpdateHealthBar(int newCurrentHealth)
{
	
    const Player *player = TheMessageMgr->GetLocalPlayer();
    if (player)
    {
         myHealth->SetValue(newCurrentHealth);
    }
}

void DisplayBoard::UpdateScore(int32 newScore)
{
	char scoreText[12];
	int safe = snprintf(scoreText, 12, "Score: %d", newScore);
	if (safe < 0)
	{
		TheEngine->Report("Update Score Error");
	}

	const Player *player = TheMessageMgr->GetLocalPlayer();
	if (player)
	{
		myScore->SetText(scoreText);
	}
}

void DisplayBoard::UpdateDeathTimer(int32 remainingTimeMilliseconds)
{
	float time = remainingTimeMilliseconds / 1000.0f; // Convert from ms to seconds
	char timeText[19];
	int safe = snprintf(timeText, 19, "Respawn in: %.2f", time);
	if (safe < 0)
	{
		TheEngine->Report("Death Time Error");
	}

	const Player *player = TheMessageMgr->GetLocalPlayer();
	if (player)
	{
		deathCounter->SetText(timeText);
	}
}

void DisplayBoard::ClearDeathTimer()
{
	const Player *player = TheMessageMgr->GetLocalPlayer();
	if (player)
	{
		deathCounter->SetText("");
	}
}

void DisplayBoard::UpdateEndingMessage(bool Winner)
{
	if (Winner)
	{
		endingMessage->SetText("Congratulations! You Win!");
	}
	else
	{
		endingMessage->SetText("Sorry, you lost! Try again!");
	}
}

void DisplayBoard::ShowPauseMenu()
{
	pauseWin->ShowWidget();
}


/*----------------------*/


StartWindow::StartWindow(const char *panelName) : GameWindow(panelName)
{
}

StartWindow::~StartWindow()
{
}


void StartWindow::PreprocessWidget(void)
{

    GameWindow::PreprocessWidget();

	hostButton = static_cast<TextButtonWidget *>(FindWidget("Host"));
	joinButton = static_cast<TextButtonWidget *>(FindWidget("Join"));
	addressText = static_cast<EditTextWidget *>(FindWidget("Address"));

	UpdatePreview();
}

void StartWindow::UpdatePreview(void)
{

}


void StartWindow::HandleWidgetEvent(Widget *widget, const WidgetEventData *eventData)
{
        EventType eventType = eventData->eventType;

        if (eventType == kEventWidgetActivate)
        {
                if (widget == hostButton)
                {
					TheGame->LoadWorld("NewWorld");
					this->HideWidget();
                }
				else if (widget == joinButton)
				{
					Command *command;
					TheGame->HandleJoinCommand(command, addressText->GetText());
					this->HideWidget();
				}
               
        }
        else if (eventType == kEventWidgetChange)
        {
        }
}
