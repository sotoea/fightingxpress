//
//  MMGamePlayer.h
//
//  Created by Martin on 2016-10-04.
//
//

#ifndef __Tombstone__MMGamePlayer__
#define __Tombstone__MMGamePlayer__

#include "TSWorld.h"
#include "TSMarkings.h"
#include "TSShakers.h"

#include "TSInterface.h"
#include "TSApplication.h"
#include "TSInput.h"
#include "TSEngine.h"

#include "MMFighter.h"


namespace MMGame
{
    using namespace Tombstone;

	/**
	* Encapsulates Gets and Sets for character players and Damage and Kill messages
	*/
    class GamePlayer : public Player
    {
        friend class Game;
        
    private:
        
        FighterController       *playerController;

        unsigned_int32          playerFlags;
        int32                   playerPing;
        
        int32                   scoreUpdateTime;
        int32                   scoreboardTime;
        
        int32                   deathTime;
        int32                   shieldTime;
        
    public:
        
        GamePlayer(PlayerKey key);
        ~GamePlayer();
        
		/**
		* Static cast for previous player
		*/
        GamePlayer *GetPreviousPlayer(void) const
        {
            return (static_cast<GamePlayer *>(Player::GetPreviousPlayer()));
        }
		/**
		* Static cast for next player
		*/
        GamePlayer *GetNextPlayer(void) const
        {
            return (static_cast<GamePlayer *>(Player::GetNextPlayer()));
        }
		/**
		* Get Player Controller
		*/
        FighterController *GetPlayerController(void) const
        {
            return (playerController);
        }
        
		/**
		* Set Controller for Fighter Player
		*/
        void SetPlayerController(FighterController *controller, const void *state = nullptr);
        
		/**
		* Handle damage on character
		*/
        CharacterStatus Damage(Fixed damage, unsigned_int32 flags, GameCharacterController *attacker = nullptr);

		/**
		* Send kill message for a player controller
		*/
        void Kill(GameCharacterController *attacker = nullptr);
        
		/**
		* 
		*/
        static PlayerKey GetAttackerKey(const GameCharacterController *attacker);
    };
    

    
}


#endif /* defined(__Tombstone__MMGamePlayer__) */
