// MODIFIED my MvM

//=============================================================


#include "MMMultiplayer.h"
#include "MMGame.h"

#include "MMFighter.h"
#include "MMGameWorld.h"

using namespace MMGame;


namespace
{
	enum
	{
		kScoreUpdateInterval			= 150,
		kScoreboardRefreshInterval		= 2000,
		kPlayerRespawnInterval			= 1500,
		kProtectionShieldInterval		= 4000
	};
}


ServerInfoMessage::ServerInfoMessage() : Message(kMessageServerInfo)
{
}

ServerInfoMessage::ServerInfoMessage(int32 numPlayers, int32 maxPlayers, const char *game, const char *world) : Message(kMessageServerInfo)
{
	playerCount = numPlayers;
	maxPlayerCount = maxPlayers;

	gameName = game;
	worldName = world;
}

ServerInfoMessage::~ServerInfoMessage()
{
}

void ServerInfoMessage::CompressMessage(Compressor& data) const
{
	data << unsigned_int16(playerCount);
	data << unsigned_int16(maxPlayerCount);
	data << gameName;
	data << worldName;
}

bool ServerInfoMessage::DecompressMessage(Decompressor& data)
{
	unsigned_int16	count, max;

	data >> count;
	data >> max;

	if (count > max)
	{
		return (false);
	}

	playerCount = count;
	maxPlayerCount = max;

	data >> gameName;
	data >> worldName;
	return (true);
}


GameInfoMessage::GameInfoMessage() : Message(kMessageGameInfo)
{
}

GameInfoMessage::GameInfoMessage(unsigned_int32 flags, const char *world) : Message(kMessageGameInfo)
{
	multiplayerFlags = flags;
	worldName = world;
}

GameInfoMessage::~GameInfoMessage()
{
}

void GameInfoMessage::CompressMessage(Compressor& data) const
{
	data << multiplayerFlags;
	data << worldName;
}

bool GameInfoMessage::DecompressMessage(Decompressor& data) 
{ 
	data >> multiplayerFlags;
	data >> worldName; 
	return (true); 
} 

bool GameInfoMessage::HandleMessage(Player *sender) const 
{ 
	if (sender->GetPlayerKey() == kPlayerServer) 
	{
		TheGame->JoinMultiplayerGame(worldName, multiplayerFlags);
	} 

	return (true);
}


PlayerStyleMessage::PlayerStyleMessage() : Message(kMessagePlayerStyle)
{
}

PlayerStyleMessage::PlayerStyleMessage(PlayerKey player, const int32 *style) : Message(kMessagePlayerStyle)
{
	playerKey = player;

	for (machine a = 0; a < kPlayerStyleCount; a++)
	{
		playerStyle[a] = style[a];
	}
}

PlayerStyleMessage::~PlayerStyleMessage()
{
}

void PlayerStyleMessage::CompressMessage(Compressor& data) const
{
	data << int16(playerKey);

	for (machine a = 0; a < kPlayerStyleCount; a++)
	{
		data << char(playerStyle[a]);
	}
}

bool PlayerStyleMessage::DecompressMessage(Decompressor& data)
{
	int16	player;

	data >> player;
	playerKey = player;

	for (machine a = 0; a < kPlayerStyleCount; a++)
	{
		char	style;

		data >> style;
		playerStyle[a] = style;
	}

	return (true);
}

bool PlayerStyleMessage::HandleMessage(Player *sender) const
{
	GamePlayer *player = static_cast<GamePlayer *>(TheMessageMgr->GetPlayer(playerKey));
	if (player)
	{
		//player->SetPlayerStyle(playerStyle);
	}

	return (true);
}


CreateModelMessage::CreateModelMessage(ModelMessageType type) : Message(kMessageCreateModel)
{
	modelMessageType = type;
}

CreateModelMessage::CreateModelMessage(ModelMessageType type, int32 index, const Point3D& position) : Message(kMessageCreateModel)
{
	modelMessageType = type;
	controllerIndex = index;
	initialPosition = position;
}

CreateModelMessage::~CreateModelMessage()
{
}

void CreateModelMessage::CompressMessage(Compressor& data) const
{
	data << char(modelMessageType);
	data << controllerIndex;
	data << initialPosition;
}

bool CreateModelMessage::DecompressMessage(Decompressor& data)
{
	// The modelMessageType value is read in Game::CreateMessage().

	data >> controllerIndex;
	data >> initialPosition;
	return (true);
}

void CreateModelMessage::InitializeModel(GameWorld *world, Model *model, Controller *controller) const
{
    /*
	controller->SetControllerIndex(controllerIndex);
	model->SetController(controller);

	model->SetNodePosition(initialPosition);
	world->AddNewNode(model);

	if (TheMessageMgr->GetSynchronizedFlag())
	{
		RigidBodyController *rigidBody = static_cast<RigidBodyController *>(controller);
		RigidBodyType type = rigidBody->GetRigidBodyType();

		if (type == kRigidBodyProjectile)
		{
			static_cast<ProjectileController *>(rigidBody)->EnterWorld(world, initialPosition);
		}
		else
            if (type == kRigidBodyCharacter)
		{
			static_cast<GameCharacterController *>(rigidBody)->EnterWorld(world, initialPosition);
		}
	}
    */
}

CreateModelMessage *CreateModelMessage::CreateMessage(ModelMessageType type)
{
	switch (type)
	{
		

		case kModelMessageSoldier:

			return (new CreateFighterMessage(type));
	}

	return (nullptr);
}




DeathMessage::DeathMessage() : Message(kMessageDeath)
{
}

DeathMessage::DeathMessage(PlayerKey player, PlayerKey attacker) : Message(kMessageDeath)
{
	playerKey = player;
	attackerKey = attacker;
}

DeathMessage::~DeathMessage()
{
}

void DeathMessage::CompressMessage(Compressor& data) const
{
	data << playerKey;
	data << attackerKey;
}

bool DeathMessage::DecompressMessage(Decompressor& data)
{
	data >> playerKey;
	data >> attackerKey;
	return (true);
}

bool DeathMessage::HandleMessage(Player *sender) const
{
		return (true);
}


ClientStyleMessage::ClientStyleMessage() : Message(kMessageClientStyle)
{
}

ClientStyleMessage::ClientStyleMessage(const int32 *style) : Message(kMessageClientStyle)
{
	for (machine a = 0; a < kPlayerStyleCount; a++)
	{
		playerStyle[a] = style[a];
	}
}

ClientStyleMessage::~ClientStyleMessage()
{
}

void ClientStyleMessage::CompressMessage(Compressor& data) const
{
	for (machine a = 0; a < kPlayerStyleCount; a++)
	{
		data << char(playerStyle[a]);
	}
}

bool ClientStyleMessage::DecompressMessage(Decompressor& data)
{
	for (machine a = 0; a < kPlayerStyleCount; a++)
	{
		char	style;

		data >> style;
		playerStyle[a] = style;
	}

	return (true);
}

bool ClientStyleMessage::HandleMessage(Player *sender) const
{
	PlayerStyleMessage styleMessage(sender->GetPlayerKey(), playerStyle);
	TheMessageMgr->SendMessage(kPlayerServer, styleMessage);
	TheMessageMgr->SendMessageClients(styleMessage, sender);
	return (true);
}


ClientOrientationMessage::ClientOrientationMessage() : Message(kMessageClientOrientation)
{
}

ClientOrientationMessage::ClientOrientationMessage(float azimuth, float altitude) : Message(kMessageClientOrientation, kMessageUnreliable)
{
	orientationAzimuth = azimuth;
	orientationAltitude = altitude;
}

ClientOrientationMessage::~ClientOrientationMessage()
{
}

void ClientOrientationMessage::CompressMessage(Compressor& data) const
{
	data << orientationAzimuth;
	data << orientationAltitude;
}

bool ClientOrientationMessage::DecompressMessage(Decompressor& data)
{
	data >> orientationAzimuth;
	data >> orientationAltitude;
	return (true);
}

bool ClientOrientationMessage::HandleMessage(Player *sender) const
{
	FighterController *controller = static_cast<GamePlayer *>(sender)->GetPlayerController();
	if (controller)
	{
		controller->UpdateOrientation(orientationAzimuth, orientationAltitude);
	}

	return (true);
}


ClientMovementMessage::ClientMovementMessage(MessageType type) : Message(type)
{
}

ClientMovementMessage::ClientMovementMessage(MessageType type, unsigned_int32 flag, float azimuth, float altitude) : Message(type)
{
	movementFlag = flag;
	movementAzimuth = azimuth;
	movementAltitude = altitude;
}

ClientMovementMessage::~ClientMovementMessage()
{
}

void ClientMovementMessage::CompressMessage(Compressor& data) const
{
	data << unsigned_int8(movementFlag);
	data << movementAzimuth;
	data << movementAltitude;
}

bool ClientMovementMessage::DecompressMessage(Decompressor& data)
{
	unsigned_int8	flag;

	data >> flag;
	movementFlag = flag;

	data >> movementAzimuth;
	data >> movementAltitude;

	return (true);
}

bool ClientMovementMessage::HandleMessage(Player *sender) const
{
	FighterController *controller = static_cast<GamePlayer *>(sender)->GetPlayerController();
	if (controller)
	{
		switch (GetMessageType())
		{
			case kMessageClientMovementBegin:
				controller->BeginMovement(movementFlag, movementAzimuth, movementAltitude);
				break;

			case kMessageClientMovementEnd:

				controller->EndMovement(movementFlag, movementAzimuth, movementAltitude);
				break;

			case kMessageClientMovementChange:

				controller->ChangeMovement(movementFlag, movementAzimuth, movementAltitude);
				break;
		}
	}

	return (true);
}


ClientFiringMessage::ClientFiringMessage(MessageType type) : Message(type)
{
}

ClientFiringMessage::ClientFiringMessage(MessageType type, float azimuth, float altitude) : Message(type)
{
	firingAzimuth = azimuth;
	firingAltitude = altitude;
}

ClientFiringMessage::~ClientFiringMessage()
{
}

void ClientFiringMessage::CompressMessage(Compressor& data) const
{
	data << firingAzimuth;
	data << firingAltitude;
}

bool ClientFiringMessage::DecompressMessage(Decompressor& data)
{
	data >> firingAzimuth;
	data >> firingAltitude;
	return (true);
}

bool ClientFiringMessage::HandleMessage(Player *sender) const
{
	
       FighterController *controller = static_cast<GamePlayer *>(sender)->GetPlayerController();
        if (controller) {
			controller->fireLaser(firingAzimuth, firingAltitude);
        }

	return (true);
}



ClientMiscMessage::ClientMiscMessage(MessageType type) : Message(type)
{
}

ClientMiscMessage::~ClientMiscMessage()
{
}

bool ClientMiscMessage::HandleMessage(Player *sender) const
{
    /*
	GamePlayer *player = static_cast<GamePlayer *>(sender);

	switch (GetMessageType())
	{
		case kMessageClientSpawn:

			if ((player->GetDeathTime() < 0))
			{
				FighterController *controller = player->GetPlayerController();
				if (!controller)
				{

					if (TheMessageMgr->GetMultiplayerFlag())
					{
						TheGame->SpawnPlayer(player);
					}
					else
					{
						TheGame->RestartWorld();
					}
				}
			}

			break;
    }
*/

	return (true);
}



/*---------------------------------------------------*/



CreateCharacterMessage:: CreateCharacterMessage(MessageType type): Message(type)
{
}


CreateCharacterMessage:: CreateCharacterMessage(MessageType type,long i, EnemyType t,PlayerKey key, Point3D p):Message(type)
{
    pos=p;
    index =i;
    chartype=t;
    ownerKey=key;
    //printf("Create Charater Message index %d type %d ownkey %d\n",index,chartype,ownerKey);
}


CreateCharacterMessage::~CreateCharacterMessage()
{
}



void CreateCharacterMessage::CompressMessage(Compressor& data) const
{
    data << index;
    data << chartype;
    data << ownerKey;
    data << pos.x;
    data << pos.y;
    data << pos.z;
}


bool CreateCharacterMessage::DecompressMessage(Decompressor& data)
{
    
    data >> index;
    data >> chartype;
    data >> ownerKey;
    data >> pos.x;
    data >> pos.y;
    data >> pos.z;
    
    return (true);
}

bool CreateCharacterMessage::HandleMessage(Player *sender) const
{
    //printf("Handle Charater Message index %d type %d ownkey %d \n",index,chartype,ownerKey);
    GameWorld *world = static_cast<GameWorld *>(TheWorldMgr->GetWorld());
    world->AddOjectAtLocation(pos,chartype,index,ownerKey);
    return(true);
    
}
/*-----------------------*/



ClientRequestMessage::ClientRequestMessage(MessageType type) : Message(type)
{
}

ClientRequestMessage::ClientRequestMessage(MessageType type, const long data ) : Message(type)
{
    myData=data;
}

ClientRequestMessage::~ClientRequestMessage()
{
}

void ClientRequestMessage::CompressMessage(Compressor& data) const
{
    data<<myData;
}

bool ClientRequestMessage::DecompressMessage(Decompressor& data)
{
    
    data >> myData;
    return (true);
}


bool ClientRequestMessage::HandleMessage(Player *sender) const
{
    //printf("Recieved ClientRequestMessage \n");
    
    //switch (GetMessageType()){
      //  case  kMessageTypeRequestAvantar:{
            //printf("ClientRequestMessage::  Requesting Avatar Player Key %d  \n",sender->GetPlayerKey());
            
            Point3D pos;
            GameWorld *world = static_cast<GameWorld *>(TheWorldMgr->GetWorld());
            world->ReqestOjectAtLocation(pos,kSoldierEntity,sender->GetPlayerKey());
			return true;
        //}
    //}
}

UpdatePlayerHealthMessage::UpdatePlayerHealthMessage() : Message(kMessageHealth)
{
}

UpdatePlayerHealthMessage::UpdatePlayerHealthMessage(int inputHealth) : Message(kMessageHealth)
{
    newHealth = inputHealth;
}

UpdatePlayerHealthMessage::~UpdatePlayerHealthMessage()
{
}

void UpdatePlayerHealthMessage::CompressMessage(Compressor& data) const
{
    data << int(newHealth);
}

bool UpdatePlayerHealthMessage::DecompressMessage(Decompressor& data)
{
    data >> newHealth;
    return (true);
}

bool UpdatePlayerHealthMessage::HandleMessage(Player *sender) const
{
    Player *player = TheMessageMgr->GetLocalPlayer();
    if (player)
    {
        if (TheDisplayBoard)
        {
            TheDisplayBoard->UpdateHealthBar(newHealth);
        }
    }
    return (true);
}

UpdatePlayerScoreMessage::UpdatePlayerScoreMessage() : Message(kMessageScore)
{
}

UpdatePlayerScoreMessage::UpdatePlayerScoreMessage(int32 inputScore) : Message(kMessageScore)
{
	newScore = inputScore;
}

UpdatePlayerScoreMessage::~UpdatePlayerScoreMessage()
{
}

void UpdatePlayerScoreMessage::CompressMessage(Compressor& data) const
{
	data << newScore;
}

bool UpdatePlayerScoreMessage::DecompressMessage(Decompressor& data)
{
	data >> newScore;
	return (true);
}

bool UpdatePlayerScoreMessage::HandleMessage(Player *sender) const
{
	Player *player = TheMessageMgr->GetLocalPlayer();
	if (player)
	{
		if (TheDisplayBoard)
		{
			TheDisplayBoard->UpdateScore(newScore);
		}
	}
	return (true);
}

UpdateGlobalRotateMessage::UpdateGlobalRotateMessage() : Message(kMessageGlobalRotate)
{
}

UpdateGlobalRotateMessage::UpdateGlobalRotateMessage(int inputGlobalRotateDirection, bool inputRotationFlag) : Message(kMessageGlobalRotate)
{
    newGlobalRotateDirection = inputGlobalRotateDirection;
    newRotationFlag = inputRotationFlag;
}

UpdateGlobalRotateMessage::~UpdateGlobalRotateMessage()
{
}

void UpdateGlobalRotateMessage::CompressMessage(Compressor& data) const
{
    data << int(newGlobalRotateDirection);
    data << bool(newRotationFlag);
}

bool UpdateGlobalRotateMessage::DecompressMessage(Decompressor& data)
{
    data >> newGlobalRotateDirection;
    data >> newRotationFlag;
    return (true);
}

bool UpdateGlobalRotateMessage::HandleMessage(Player *sender) const
{
    Player *player = TheMessageMgr->GetLocalPlayer();
    if (player)
    {        
        FighterController *controller = static_cast<const GamePlayer *>(player)->GetPlayerController();
        controller->SetCurrentGlobalRotationDirection(newGlobalRotateDirection);
        controller->SetCurrentRotationFlag(newRotationFlag);
    }
    return (true);
}

UpdateClientMapRotationMessage::UpdateClientMapRotationMessage() : Message(kMessageUpdateMapRotation)
{
}

UpdateClientMapRotationMessage::UpdateClientMapRotationMessage(Transform4D inputServerMapRotation, int inputGlobalRotationDirection) : Message(kMessageUpdateMapRotation)
{
    newClientMapRotation = inputServerMapRotation;
    newGlobalRotationDirection = inputGlobalRotationDirection;
}

UpdateClientMapRotationMessage::~UpdateClientMapRotationMessage()
{
}

void UpdateClientMapRotationMessage::CompressMessage(Compressor& data) const
{
    data << newClientMapRotation[0][0];
    data << newClientMapRotation[0][1];
    data << newClientMapRotation[0][2];
    data << newClientMapRotation[0][3];
    data << newClientMapRotation[1][0];
    data << newClientMapRotation[1][1];
    data << newClientMapRotation[1][2];
    data << newClientMapRotation[1][3];
    data << newClientMapRotation[2][0];
    data << newClientMapRotation[2][1];
    data << newClientMapRotation[2][2];
    data << newClientMapRotation[2][3];
    data << newClientMapRotation[3][0];
    data << newClientMapRotation[3][1];
    data << newClientMapRotation[3][2];
    data << newClientMapRotation[3][3];
    data << newGlobalRotationDirection;
}

RespawnPlayerMessage::RespawnPlayerMessage() : Message(kMessageRespawn)
{
}

RespawnPlayerMessage::RespawnPlayerMessage(PlayerKey key, unsigned_int32 fighterFlags, Point3D resPos) : Message(kMessageRespawn)
{
	playerKey = key;
	newFighterFlags = fighterFlags;
	respawnPos = resPos;
}

RespawnPlayerMessage::~RespawnPlayerMessage()
{
}

void RespawnPlayerMessage::CompressMessage(Compressor& data) const
{
	data << playerKey;
	data << newFighterFlags;
	data << respawnPos;

}

bool RespawnPlayerMessage::DecompressMessage(Decompressor& data)
{
	data >> playerKey;
	data >> newFighterFlags;
	data >> respawnPos;

	return (true);
}

bool RespawnPlayerMessage::HandleMessage(Player *sender) const
{
	GamePlayer* player = static_cast<GamePlayer *>(TheMessageMgr->GetPlayer(playerKey));
	if (player)
	{
		FighterController* controller = player->GetPlayerController();
		if (controller)
		{
			controller->SetFighterFlags(newFighterFlags);
			controller->SetRigidBodyPosition(respawnPos);
		}
		if (player == TheMessageMgr->GetLocalPlayer())
		{
			TheDisplayBoard->ClearDeathTimer();
		}
	}
	return (true);
}

GameOverMessage::GameOverMessage() : Message(kMessageGameOver)
{
}

GameOverMessage::GameOverMessage(PlayerKey winnerPlayerKey) : Message(kMessageGameOver)
{
	winner = winnerPlayerKey;
}

GameOverMessage::~GameOverMessage()
{
}

void GameOverMessage::CompressMessage(Compressor& data) const
{
	data << winner;

}

bool GameOverMessage::DecompressMessage(Decompressor& data)
{
	data >> winner;

	return (true);
}

bool GameOverMessage::HandleMessage(Player *sender) const
{
	TheDisplayBoard->UpdateEndingMessage(TheMessageMgr->GetLocalPlayerKey() == winner);
	FighterController *controller = static_cast<GamePlayer *>(sender)->GetPlayerController();
	controller->endTime = TheTimeMgr->GetAbsoluteTime();
	return (true);
}
bool UpdateClientMapRotationMessage::DecompressMessage(Decompressor& data)
{
    data >> newClientMapRotation[0][0];
    data >> newClientMapRotation[0][1];
    data >> newClientMapRotation[0][2];
    data >> newClientMapRotation[0][3];
    data >> newClientMapRotation[1][0];
    data >> newClientMapRotation[1][1];
    data >> newClientMapRotation[1][2];
    data >> newClientMapRotation[1][3];
    data >> newClientMapRotation[2][0];
    data >> newClientMapRotation[2][1];
    data >> newClientMapRotation[2][2];
    data >> newClientMapRotation[2][3];
    data >> newClientMapRotation[3][0];
    data >> newClientMapRotation[3][1];
    data >> newClientMapRotation[3][2];
    data >> newClientMapRotation[3][3];
    data >> newGlobalRotationDirection;
    return (true);
}

bool UpdateClientMapRotationMessage::HandleMessage(Player *sender) const
{
    Player *player = TheMessageMgr->GetLocalPlayer();
    if (player)
    {
        FighterController *controller = static_cast<const GamePlayer *>(player)->GetPlayerController();
        controller->SetCurrentMapRotation(newClientMapRotation, newGlobalRotationDirection);
    }
    return (true);
}
// ZUXSVMT
