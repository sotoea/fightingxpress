//
//  MMGameWorld.cpp
//
//  Created by Martin on 2016-10-04.
//
//

#include <stdio.h>
#include "MMGameWorld.h"
#include "MMMultiPLayer.h"
#include "MMGamePlayer.h"
#include "MMFighter.h"
#include "MMGame.h"






using namespace MMGame;


void GameWorld::MoveWorld(void)
{
	World::MoveWorld();
	//printf("TIck WOrld \n");
}





GameWorld::GameWorld(const char *name) :
World(name),
spectatorCamera(1.,1.0F, 0.3F)
{
}

GameWorld::~GameWorld()
{
    
}

WorldResult GameWorld::PreprocessWorld(void)
{
    WorldResult result = World::PreprocessWorld();
    if (result != kWorldOkay)
    {
        return (result);
    }
    
    SetWorldCamera(&spectatorCamera);
    playerCamera = &firstPersonCamera;
    
    spawnLocatorCount = 0;
    CollectZoneMarkers(GetRootNode());
    
    {
        spectatorCamera.SetNodePosition(Point3D(0.0F, 0.0F, 1.0F));
    }
    
    bloodIntensity = 0.0F;
    return (kWorldOkay);
}

void GameWorld::CollectZoneMarkers(Zone *zone)
{

    Marker *marker = zone->GetFirstMarker();
    while (marker)
    {
        Marker *next = marker->GetNextListElement();
        
        if ((marker->GetMarkerType() == kMarkerLocator) && (marker->NodeEnabled()))
        {
            LocatorMarker *locator = static_cast<LocatorMarker *>(marker);
            switch (locator->GetLocatorType())
            {
                case kLocatorSpawn:
                    spawnLocatorCount++;
                    spawnLocatorList.AppendListElement(locator);
                    break;
            }
        }
        
        marker = next;
    }
    
    Zone *subzone = zone->GetFirstSubzone();
    while (subzone)
    {
        CollectZoneMarkers(subzone);
        subzone = subzone->GetNextListElement();
    }
}

RigidBodyStatus GameWorld::HandleNewRigidBodyContact(RigidBodyController *rigidBody, const RigidBodyContact *contact, RigidBodyController *contactBody)
{
    return (kRigidBodyUnchanged);
}

void GameWorld::DetectInteractions(void)
{
    
    
    World::DetectInteractions();
}

void GameWorld::BeginRendering(void)
{
    World::BeginRendering();
}

void GameWorld::EndRendering(FrameBuffer *frameBuffer)
{
    World::EndRendering(frameBuffer);
    
    
}

void GameWorld::SetCameraTargetModel(Model *model)
{
    firstPersonCamera.SetTargetModel(model);
    chaseCamera.SetTargetModel(model);
    SetWorldCamera(playerCamera);
    
}

void GameWorld::SetSpectatorCamera(const Point3D& position, float azm, float alt)
{
    firstPersonCamera.SetTargetModel(nullptr);
    chaseCamera.SetTargetModel(nullptr);
    SetWorldCamera(&spectatorCamera);
    spectatorCamera.SetNodePosition(position);
    spectatorCamera.SetCameraAzimuth(azm);
    spectatorCamera.SetCameraAltitude(alt);
}

void GameWorld::SetLocalPlayerVisibility(void)
{
    
}

void GameWorld::ChangePlayerCamera(void)
{
    const Player *player = TheMessageMgr->GetLocalPlayer();
    if ((player) && (static_cast<const GamePlayer *>(player)->GetPlayerController()))
    {
        if (playerCamera == &firstPersonCamera)
        {
            playerCamera = &chaseCamera;
        }
        else
        {
            playerCamera = &firstPersonCamera;
        }
        
        
        SetWorldCamera(playerCamera);
        SetLocalPlayerVisibility();
    }
}

void GameWorld::SetFocalLength(float focal)
{
    spectatorCamera.GetObject()->SetFocalLength(focal);
    firstPersonCamera.GetObject()->SetFocalLength(focal);
    chaseCamera.GetObject()->SetFocalLength(focal);
}

/*------------------------------------------*/

// THis is an "NICE" way to convert constants into strings for debugging !!

#define TYPE_NAME(type) \
(kSoldierEntity       == type ? "kSoldierEntity"    :  (0 == type ? "yellow" : "unknown"))


/*----------------------------------------------------------------------------------*/

// HERE ARE THE FUNCTIONS FOR RUNTIME DISTRIBUTED SCENE GRAPH MANAGEMENT
// ADDING ANYHTING TO THE SCENE GRAPH WE REQUEST IT AND THEN A MESSAGE IS SEND TO ALL
// PARTICIPANTS TO ADD IT !


void GameWorld::ReqestOjectAtLocation(const Point3D& pos ,int type,PlayerKey key)
{
    
    // NO CALLS TP THIS BUT ON SERVER !!!!!
    
    // VERY IMPROTANT !! THIS IS THE UNIQUE CONTROLLER INDEX
    long cIndex=TheWorldMgr->GetWorld()->NewControllerIndex();
    //PlayerKey key=TheMessageMgr->GetLocalPlayer()->GetPlayerKey();
    
    
    //printf("Request Oject: Index %d Type %s key %d \n",cIndex,TYPE_NAME(type),key);
    if(type==kSoldierEntity){
        printf("THis is an Avatar, Spwn a spawn point \n");
        GameWorld *world = static_cast<GameWorld *>(TheWorldMgr->GetWorld());
        const Marker *marker = world->GetSpawnLocator(0);
        
        CreateCharacterMessage  message(kMessageCreateCharacter,cIndex,type,key,marker->GetWorldPosition());
        TheMessageMgr->SendMessageAll(message,true);

    }else{
        CreateCharacterMessage  message(kMessageCreateCharacter,cIndex,type,key,pos);
        TheMessageMgr->SendMessageAll(message,true);

        
    }
    
 }



/*----------------------------------------------------------------------------------*/

Controller* GameWorld::CreateAvatar(const Point3D& pos ,long index,PlayerKey key)
{
    
    GameWorld *world = static_cast<GameWorld *>(TheWorldMgr->GetWorld());
    //const Marker *locator = world->GetSpawnLocator(0);
    
    
    // Calculate the angle corresponding to the direction the character is initially facing.
    //const Vector3D direction = locator->GetWorldTransform()[0];
    //float azimuth = atan2(direction.y, direction.x);
    
    
    Model *model = Model::GetModel(kModelSoldier);
    FighterController *contr = new FighterController(kSoldierEntity);
    model->SetController(contr);
    
    // SET INDEX AND KEY !!! AND FOR THE PLAYER !!!!!!!!!!!!
    contr->SetPlayerKey(key);
    GamePlayer* player = static_cast<GamePlayer *>(TheMessageMgr->GetPlayer(key));
    player->SetPlayerController(contr, nullptr);
    
    
    // NOW WE Have to set the camara to this avantar if it is the local!
    if(key==TheMessageMgr->GetLocalPlayer()->GetKey()){
        
        printf("This is my player, Set Camara \n");
        
        
        world->SetCameraTargetModel(model);
        world->ChangePlayerCamera();
        
    }
    
    // I Do nto need these here actually
    contr->SetControllerIndex(index);
    model->SetNodePosition(pos);
    world->AddNewNode(model);

    return(contr);
}


/*----------------------------------------------------------------------------------*/


void GameWorld::AddOjectAtLocation(const Point3D& pos ,int type,long index,PlayerKey key)
{
    
    //printf("CREATE object of type %s with index %d for player key %d \n",TYPE_NAME(type),index,key);
    //printf("CREATE  Oject: Index %d Type %s key %d \n",index,TYPE_NAME(type),key);
    
    // Create the controller and the model and put it together
    Controller* controller;
    Model *model;
    
    
    // TMP
    //if (key =! TheMessageMgr->GetLocalPlayer())return;
    
    GameWorld *world = static_cast<GameWorld *>(TheWorldMgr->GetWorld());
    
    switch(type){
        case kSoldierEntity:{
                // SPECIAL CASE !!!!
                CreateAvatar(pos , index, key);return;
                return;
        }
    default:
            printf("Ukowen Object Type \n");
            return;

    }
    
    // SET THE CONTROLLER KEY
    model->SetController(controller);
    controller->SetControllerIndex(index);
    
    // Put the model in the world at the locator's position.
    
    Node* node=controller->GetTargetNode();
    node->SetNodePosition(pos);
    world->AddNewNode(node);
    
}

/*----------------------------------------------------------*/

void GameWorld::PopulateWorld(void)
{

}

/*-----------------------------------*/


FighterController* GameWorld::GetLocalAvantar(void)
{
        GamePlayer* player = static_cast< GamePlayer *>(TheMessageMgr->GetLocalPlayer());
        return(player->GetPlayerController());
}

#define k_FloatInfinity 100000.f;


Point3D  GameWorld::GetClosesedAvantarPosition(Point3D pos)
{
        float tmp = k_FloatInfinity;
        Point3D aPos,outPos = Point3D(0,0,0);
        float dist;

        GamePlayer* player= static_cast< GamePlayer *>(TheMessageMgr->GetFirstPlayer());
        while(player!=nullptr){
                FighterController* avantar=player->GetPlayerController();
                if(avantar!=nullptr){
                                aPos=avantar->GetPosition();
                                dist=Magnitude(pos-aPos);
                                if(dist<tmp){outPos=aPos;tmp=dist;}
                }
                player=player->GetNextPlayer();
        }
        return(outPos);
}
/*--------- HANDLE ALL CONTACTS HERE --*/


void GameWorld::Contact(const RigidBodyContact *contact, RigidBodyController *body1,RigidBodyController *c_body)
{
    printf("Handle COntact \n");
    //
    if (body1->GetControllerType() == kSoldierEntity    ){
        printf("WIth Fighter \n");
    }
    /*
     Point3D         worldPosition;
     Vector3D        worldNormal;
     
     contact->GetWorldContactPosition(this, &worldPosition, &worldNormal);
     
     if (contactBody->GetRigidBodyType() == kRigidBodyProjectile)
     {
     static_cast<ProjectileController *>(contactBody)->Destroy(worldPosition, worldNormal);
     }
     
     return (kRigidBodyDestroyed);
     
     */

}



