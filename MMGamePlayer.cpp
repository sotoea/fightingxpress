

#include "MMGamePlayer.h"
#include "MMGame.h"
#include "MMFighter.h"

using namespace MMGame;


GamePlayer::GamePlayer(PlayerKey key) : Player(key)
{
    playerController = nullptr;
    
    scoreUpdateTime = 0;
    deathTime = -1;
    shieldTime = -1;
    
}

GamePlayer::~GamePlayer()
{
}

void GamePlayer::  SetPlayerController(FighterController *controller, const void *state)
{
    playerController = controller;
    if (controller)
    {
        controller->SetFighterPlayer(this);

    }
}


CharacterStatus GamePlayer::Damage(Fixed damage, unsigned_int32 flags, GameCharacterController *attacker)
{

    return (kCharacterUnaffected);
}

void GamePlayer::Kill(GameCharacterController *attacker)
{
    if (TheMessageMgr->GetServerFlag())
    {
        
        if (playerController)
        {
            TheMessageMgr->SendMessageAll(ControllerMessage(FighterController::kFighterMessageDeath, playerController->GetControllerIndex()));
            TheMessageMgr->SendMessageAll(DeathMessage(GetPlayerKey(), GetAttackerKey(attacker)));
            
            scoreUpdateTime = 0;
            scoreboardTime = 0;

        }
    }
}

PlayerKey GamePlayer::GetAttackerKey(const GameCharacterController *attacker)
{
    if ((attacker) && (attacker->GetCharacterType() == kCharacterPlayer))
    {
        //return (static_cast<const FighterController *>(attacker)->GetFighterPlayer()->GetPlayerKey());
    }
    
    return (kPlayerNone);
}



