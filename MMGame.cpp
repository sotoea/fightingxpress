
// MODIFIED my MvM


//=============================================================
//
// Tombstone Engine version 1.0
// Copyright 2016, by Terathon Software LLC
//
// This file is part of the Tombstone Engine and is provided under the
// terms of the license agreement entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================

#include <stdio.h>
#include "TSToolWindows.h"
#include "TSAudioCapture.h"
#include "TSWater.h"

#include "MMGame.h"
#include "MMMultiplayer.h"
#include "MMCameras.h"
#include "MMGameWorld.h"
#include "MMFighter.h"



using namespace MMGame;



Game *MMGame::TheGame = nullptr;



Game::Game() :
	Global<Game>(TheGame),

	controllerCreator(&CreateController),


	spawnLocatorRegistration(kLocatorSpawn, "Spawn Location"),
	soldierModelReg(kModelSoldier, nullptr, "soldier/Soldier", kModelPrecache | kModelPrivate, kControllerSoldier),

	hostCommandObserver(this, &Game::HandleHostCommand),
	joinCommandObserver(this, &Game::HandleJoinCommand),


	hostCommand("host", &hostCommandObserver),
	joinCommand("join", &joinCommandObserver),

	forwardAction(kActionForward, kMovementForward, kSpectatorMoveForward),
	backwardAction(kActionBackward, kMovementBackward, kSpectatorMoveBackward),
	leftAction(kActionLeft, kMovementLeft, kSpectatorMoveLeft),
	rightAction(kActionRight, kMovementRight, kSpectatorMoveRight),
	upAction(kActionUp, kMovementUp, kSpectatorMoveUp),
	pauseAction(kActionSave),
		downAction(kActionDown, kMovementDown, kSpectatorMoveDown),
		movementAction(kActionMovement, 0, 0),
		primaryFireAction(kActionFirePrimary)

{
	//TheDisplayMgr->InstallDisplayEventHandler(&displayEventHandler);

	TheEngine->AddCommand(&hostCommand);
	TheEngine->AddCommand(&joinCommand);
	

	TheDisplayMgr->SetDisplayMode(Integer2D(1920,1080), kDisplayFullscreen);
	TheInputMgr->AddAction(&forwardAction);
	TheInputMgr->AddAction(&backwardAction);
	TheInputMgr->AddAction(&leftAction);
	TheInputMgr->AddAction(&rightAction);
	TheInputMgr->AddAction(&upAction);
	TheInputMgr->AddAction(&downAction);
	TheInputMgr->AddAction(&movementAction);
	TheInputMgr->AddAction(&primaryFireAction);
	TheInputMgr->AddAction(&pauseAction);

	prevEscapeCallback = TheInputMgr->GetEscapeCallback();
	prevEscapeCookie = TheInputMgr->GetEscapeCookie();
	TheInputMgr->SetEscapeCallback(&EscapeCallback, this);

	TheWorldMgr->SetWorldCreator(&CreateWorld);
	TheMessageMgr.SetPlayerCreator(&CreatePlayer);
	gameFlags = 0;

	currentWorldName[0] = 0;


	TheInterfaceMgr->SetInputManagementMode(kInputManagementAutomatic);

	TheDisplayBoard->OpenBoard();
	TheWorldMgr.LoadWorld("empty");

	//TheGame->LoadWorld("NewWorld");
}

Game::~Game()
{
	ExitCurrentGame();
    //boardList.PurgeList();

	TheWorldMgr->SetWorldCreator(nullptr);
	TheMessageMgr->SetPlayerCreator(nullptr);

	TheInputMgr->SetEscapeCallback(prevEscapeCallback, prevEscapeCookie);
}

World *Game::CreateWorld(const char *name, void *cookie)
{
	return (new GameWorld(name));
}

Player *Game::CreatePlayer(PlayerKey key, void *cookie)
{
	return (new GamePlayer(key));
}

Controller *Game::CreateController(Unpacker& data)
{

	return (nullptr);
}



void Game::EscapeCallback(void *cookie)
{
}





void Game::InitPlayerStyle(int32 *style)
{
}

void Game::HandleHostCommand(Command *command, const char *text)
{
	if (*text != 0)
	{
		ResourceName	name;

		Text::ReadString(text, name, kMaxResourceNameLength);
		HostMultiplayerGame(name, 0);
	}
}


void Game::StartSinglePlayerGame(const char *name)
{
    ExitCurrentGame();
    
    currentWorldName = name;
    
    HostMultiplayerGame(name,0);
    
    TheDisplayBoard->UpdateScore(0);
    
    //World *world = new GameWorld(name);
}


// I hard coded this, so it connects to the local machine
void Game::HandleJoinCommand(Command *command, const char *text)
{
    printf(" Joining a Game \n");
    TheMessageMgr->BeginMultiplayerGame(false);
    
    //String *addressText=MessageMgr::AddressToString(TheNetworkMgr->GetLocalAddress());
    
    // We'll first want to provide the user with some feedback - so he'll know what he's doing.
    
    NetworkAddress address = MessageMgr::StringToAddress(text); //TODO: If invalid address entered connect local
    if (address.GetPort() == 0)address.SetPort(kGamePort);

    TheNetworkMgr->SetPortNumber(kGamePort);
    TheNetworkMgr->SetBroadcastPortNumber(kGamePort);
    TheNetworkMgr->Initialize();
    
    // Now we're just going to (try to) connect to the entered address.
    NetworkAddress local_addr = TheNetworkMgr->GetLocalAddress();
    local_addr.SetPort(kGamePort);
    
    //TheMessageMgr->Connect(local_addr);
    TheMessageMgr->Connect(address);
    
    TheDisplayBoard->UpdateScore(0);
}



EngineResult Game::HostMultiplayerGame(const char *name, unsigned_int32 flags)
{
	ExitCurrentGame();
    
    printf("Start Multi Player Game \n");

	//TheNetworkMgr->SetProtocol(kGameProtocol);
	TheNetworkMgr->SetPortNumber(kGamePort);
	TheNetworkMgr->SetBroadcastPortNumber(kGamePort);
    TheNetworkMgr->Initialize();

	EngineResult result = TheMessageMgr->BeginMultiplayerGame(true);
	if (result == kNetworkOkay)
	{
		result = TheWorldMgr->LoadWorld(name);
		if (result == kWorldOkay)
		{
            
            printf("World Loaded \n");
            GameWorld *world = static_cast<GameWorld *>(TheWorldMgr->GetWorld());
			//multiplayerFlags = flags;

			GamePlayer *player = static_cast<GamePlayer *>(TheMessageMgr->GetLocalPlayer());
			//unsigned_int32 playerFlags = (flags & kMultiplayerDedicated) ? kPlayerInactive : 0;
            
			//player->SetPlayerFlags(playerFlags);
            const Point3D pos= Point3D(0.0F, 0.0F, 2.0F);
            
            // Request an Avatar  ONLY ON SERVER !!!!!
            
            world->ReqestOjectAtLocation(pos ,kSoldierEntity,player->GetPlayerKey());
            
            // POPULATE ONLY ON SERVER !!!
            world->PopulateWorld();
            
            //DisplayBoard::OpenBoard();
            //TheDisplayBoard->ShowMessageText("Server");

		}
	}

	return (result);
}

EngineResult Game::JoinMultiplayerGame(const char *name, unsigned_int32 flags)
{

	EngineResult result = TheWorldMgr->LoadWorld(name);
	if (result == kWorldOkay)
	{

        printf("Joinging Game in word %s \n",name);
		//GamePlayer *player = static_cast<GamePlayer *>(TheMessageMgr->GetPlayer(kPlayerServer));
		//if (flags & kMultiplayerDedicated)
		{
			//player->SetPlayerFlags(player->GetPlayerFlags() | kPlayerInactive);
		}
        DisplayBoard::OpenBoard();

	}

	return (result);
}

void Game::ExitCurrentGame(void)
{

	TheMessageMgr->EndGame();
 //   delete TheDisplayBoard;

	//gameFlags = 0;

	TheWorldMgr->UnloadWorld();

}

void Game::RestartWorld(void)
{
	GameWorld *world = static_cast<GameWorld *>(TheWorldMgr->GetWorld());
	if ((world) && (world->GetSpawnLocatorCount() != 0))
	{
		StartSinglePlayerGame(currentWorldName);
	}
}

EngineResult Game::LoadWorld(const char *name)
{
	StartSinglePlayerGame(name);
	return (kEngineOkay);
}

void Game::UnloadWorld(void)
{
	ExitCurrentGame();
	
}



void Game::HandleConnectionEvent(ConnectionEvent event, const NetworkAddress& address, const void *param)
{
    printf("HandleConnectionEvent\n");
    switch (event)
    {
        case kConnectionQueryReceived:
        {
            World *world = TheWorldMgr->GetWorld();
            if (world){
                printf("SENDING WORLD NAME \n");
                const char *gameName = TheEngine->GetVariable("gameName")->GetValue();
                const char *worldName = world->GetWorldName();
                
                ServerInfoMessage message(TheMessageMgr->GetPlayerCount(), TheMessageMgr->GetMaxPlayerCount(), gameName, worldName);
                TheMessageMgr->SendConnectionlessMessage(address, message);
            }
            
            break;
        }
            
        case kConnectionAttemptFailed:
            printf("kConnectionAttemptFailed\n");
            
            // The server rejected our connection.
            
            
            break;
            
        case kConnectionServerAccepted:
            
            // The server accepted our connection.
            printf("kConnectionServerAccepted\n");
            
            
            
            // Tell the server what player styles the user has set.
            // The server will forward this information to the other players.
            
            //TheMessageMgr->SendMessage(kPlayerServer, ClientStyleMessage(static_cast<GamePlayer *>(TheMessageMgr->GetLocalPlayer())->GetPlayerStyle()));
            break;
            
        case kConnectionServerClosed:
            
            // The server was shut down.
            printf("closed \n");
            
            ExitCurrentGame();
            break;
            
        case kConnectionServerTimedOut:
            
            // The server has stopped responding.
            
            ExitCurrentGame();
            break;
    }
    
    Application::HandleConnectionEvent(event, address, param);
}

void Game::HandlePlayerEvent(PlayerEvent event, Player *player, const void *param)
{
    printf("HandlePlayerEvent\n");
    switch (event)
    {
        case kPlayerConnected:
        {
            printf("kPlayerConnected\n");
            
            if (TheMessageMgr->GetSynchronizedFlag())
            {
		 // NOTHING TO DO 
            }
            
            if (TheMessageMgr->GetServerFlag())
            {
                /*
                 GamePlayer *gamePlayer = static_cast<GamePlayer *>(TheMessageMgr->GetFirstPlayer());
                 while (gamePlayer)
                 {
                 if ((gamePlayer != player) && (gamePlayer->GetPlayerFlags() & kPlayerReceiveVoiceChat))
                 {
                 new Channel(player, gamePlayer);
                 }
                 
                 gamePlayer = gamePlayer->GetNextPlayer();
                 }
                 */
            }
            
            break;
        }
            
        case kPlayerDisconnected:
        {
             printf("kPlayerDisconnected\n");
            Controller *controller = static_cast<GamePlayer *>(player)->GetPlayerController();
            if (controller)
            {
                //delete controller->GetTargetNode();
            }
            
            break;
        }
            
        case kPlayerTimedOut:
        {
            printf("kPlayerTimedOut\n");
            Controller *controller = static_cast<GamePlayer *>(player)->GetPlayerController();
            if (controller)
            {
                //delete controller->GetTargetNode();
            }
            
            break;
        }
            
        case kPlayerInitialized:
        {
            
            printf("kPlayerInitialized\n");
            // A new player joining the game has been initialized. For each player already
            // in the game, send a message  TO constrcut the coresponding Avatar
            
            const GamePlayer *gamePlayer = static_cast<GamePlayer *>(TheMessageMgr->GetFirstPlayer());
            do
            {
                if (gamePlayer != player)
                {
			// NOTHING TO DO
                }
                
                gamePlayer = gamePlayer->GetNextPlayer();
            } while (gamePlayer);
            
            // Now tell the new player what world is being played.
            
            World *world = TheWorldMgr->GetWorld();
            printf("Server Send Word Name \n");
            if (world) player->SendMessage(GameInfoMessage(0,world->GetWorldName()));
            
            
            break;
        }
            
            
    }
    
    Application::HandlePlayerEvent(event, player, param);
}

// THIS IS CALLED WHEN ALL THE LOADING ON THE CLIENT IS DONE
void Game::HandleGameEvent(GameEvent event, const void *param)
{
    printf("HandleGameEvent\n");
    switch (event)
    {
        case kGameSynchronized:
            printf("Game Syncronized request avatar\n");
            ClientRequestMessage message(kMessageRequestAvantar,0);
            TheMessageMgr->SendMessage(kPlayerServer,message);
            break;
    }
}

// Add all messages so they can be send over the network !!
Message *Game::CreateMessage(MessageType type, Decompressor& data) const
{
    switch (type)
    {
        case kMessageServerInfo:
            
            return (new ServerInfoMessage);
            
        case kMessageGameInfo:
            
            return (new GameInfoMessage);
            
        case kMessagePlayerStyle:
            
            return (new PlayerStyleMessage);
            
        case kMessageCreateModel:
        {
            unsigned_int8	modelType;
            
            data >> modelType;
            return (CreateModelMessage::CreateMessage(modelType));
        }
            
            
        case kMessageDeath:
            
            return (new DeathMessage);
            
            
        case kMessageClientStyle:
            
            return (new ClientStyleMessage);
            
        case kMessageClientOrientation:
            
            return (new ClientOrientationMessage);
            
        case kMessageClientMovementBegin:
        case kMessageClientMovementEnd:
        case kMessageClientMovementChange:
            
            return (new ClientMovementMessage(type));
            
        case kMessageClientFiringPrimaryBegin:
        //case kMessageClientFiringEnd:
            
            return (new ClientFiringMessage(type));
            
        case kMessageCreateCharacter:
            return(new CreateCharacterMessage(type));
            
        case kMessageRequestAvantar:
            
            return(new ClientRequestMessage(type));
        case kMessageHealth:
            return(new UpdatePlayerHealthMessage);
        case kMessageGlobalRotate:
            return(new UpdateGlobalRotateMessage);
        case kMessageUpdateMapRotation:
            return(new UpdateClientMapRotationMessage);
		case kMessageScore:
			return(new UpdatePlayerScoreMessage);
		case kMessageRespawn:
			return(new RespawnPlayerMessage);
		case kMessageGameOver:
			return(new GameOverMessage);
            
    }
    
    return (nullptr);
}

void Game::ReceiveMessage(Player *sender, const NetworkAddress& address, const Message *message)
{
    
}

void Game::SpawnPlayer(Player *player)
{
    GameWorld *world = static_cast<GameWorld *>(TheWorldMgr->GetWorld());
    if (world)
    {
        int32 count = world->GetSpawnLocatorCount();
        if (count != 0)
        {
            //const Marker *marker = world->GetSpawnLocator(Math::RandomInteger(count));
            
            //const Vector3D direction = marker->GetWorldTransform()[0];
            //float azimuth = Arctan(direction.y, direction.x);
            
            //int32 fighterIndex = world->NewControllerIndex();
            //int32 weaponIndex = world->NewControllerIndex();
            //TheMessageMgr->SendMessageAll(CreateGusGravesMessage(fighterIndex, marker->GetWorldPosition(), azimuth, //0.0F, 0, kWeaponPistol, weaponIndex, player->GetPlayerKey()));
            
        }
    }
}


void Game::ApplicationTask(void)
{
}


