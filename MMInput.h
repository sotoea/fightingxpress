//=============================================================
//
// Tombstone Engine version 1.0
// Copyright 2016, by Terathon Software LLC
//
// This file is part of the Tombstone Engine and is provided under the
// terms of the license agreement entered by the registed user.
//
// Unauthorized redistribution of source code is strictly
// prohibited. Violators will be prosecuted.
//
//=============================================================


#ifndef MMInput_h
#define MMInput_h


#include "TSInput.h"
#include "MMBase.h"


namespace MMGame
{
    
    using namespace Tombstone;
    
    
    enum : ActionType
    {
        kActionForward				= 'frwd',
		kActionBackward				= 'bkwd',
		kActionLeft					= 'left',
		kActionRight				= 'rght',
		kActionUp					= 'jump',
		kActionDown					= 'down',
		kActionMovement				= 'move',
		kActionHorizontal			= 'horz',
		kActionVertical				= 'vert',
		kActionFirePrimary			= 'fire',
		//kActionFirePrimary			= 'trig',
		kActionFireSecondary		= 'trig',
		kActionUse					= 'uobj',
		kActionPistol				= 'pist',
		kActionShotgun				= 'shgn',
		kActionCrossbow				= 'cbow',
		kActionSpikeShooter			= 'spsh',
		kActionGrenadeLauncher		= 'gren',
		kActionQuantumCharger		= 'qchg',
		kActionRocketLauncher		= 'rock',
		kActionPlasmaGun			= 'plas',
		kActionProtonCannon			= 'pcan',
		kActionSpecialWeapon		= 'spec',
		kActionNextWeapon			= 'next',
		kActionPrevWeapon			= 'prev',
		kActionFlashlight			= 'lite',
		kActionCameraView			= 'camr',
		kActionScoreboard			= 'scor',
		kActionChat					= 'mess',
		kActionLoad					= 'load',
		kActionSave					= 'save'
	};


	enum
	{
		kMovementForward			= 1 << 0,
		kMovementBackward			= 1 << 1,
		kMovementLeft				= 1 << 2,
		kMovementRight				= 1 << 3,
		kMovementUp					= 1 << 4,
		kMovementDown				= 1 << 5,
		kMovementPlanarMask			= 15
	};
    

    /**
     * An Action class dedicated to handling all player movement related events.
     */

	class MovementAction : public Action
	{
		private:

			unsigned_int32		movementFlag;
			unsigned_int32		spectatorFlag;

		public:

			MovementAction(ActionType type, unsigned_int32 moveFlag, unsigned_int32 specFlag);
			~MovementAction();
        
        /**
         * Handles when a button dedicated to any MovementAction is initially pressed.
         */
			void HandleEngage(void);
        /**
         * Handles when a button dedicated to any MovementAction is finished pressed.
         */
			void HandleDisengage(void);
        /**
         * Updates local players movement flags and sends the updated flags to the server.
         */
			void HandleMove(int32 value);
	};

    /**
     * An Action class dedicated to handling all gun fire related events.
     */
	class FireAction : public Action
	{
		public:

			FireAction(ActionType type);
			~FireAction();
        
        
        /**
         * Handles when a button dedicated to any FireAction is initially pressed.
         */
        void HandleEngage(void);
	};
    
    /**
     * An Action class dedicated to handling all menu related events.
     */
	class MenuAction : public Action
	{
	public:

		MenuAction(ActionType type);
		~MenuAction();
        
        /**
         * Handles when a button dedicated to any MenuAction is initially pressed.
         */
		void HandleEngage(void);
	};
}


#endif
