
#ifndef C4_MMSkin_h
#define C4_MMSkin_h

#include "TSInterface.h"





namespace MMGame
{
    
    using namespace Tombstone;

	enum : WidgetType
	{
		kWidgetInput = 'inpt'
	};

    
    class GameBoard : public Board, public ListElement<GameBoard>
    {
    protected:
        
        GameBoard(){};
        
    public:
        
        ~GameBoard(){};
    };




	class GameWindow : public Window, public ListElement<GameWindow>
	{
	protected:

		GameWindow(const char *panelName);

	public:

		virtual ~GameWindow();
	};


	/**
	 * Class DisplayBoard encapsulates all widgets to be displayed on the screen along with their behaviour
	 */
    
    class DisplayBoard : public GameBoard , public Global<DisplayBoard>
 {
    private:
        
               
        TextWidget                      *scoreText;
        TextWidget                      *healthText;
    
        
        Widget                          healthGroup;
        Widget                          scoreGroup;
        Widget                          weaponsGroup;

		Window							*pauseWin;

		Image							*pic;
		TextWidget						*myScore;
		TextWidget						*deathCounter;
		TextWidget						*endingMessage;
		ImageWidget						*reticle;

		ProgressWidget                  *myHealth;
		Widget							*startBut;
		ImageWidget						*startPic;
        
        static ColorRGBA CalculateBarColor(float value);
        
        
    public:
        
        DisplayBoard();
        ~DisplayBoard();
		/**
		* If no DisplayBoard is initiated, creates and adds a DisplayBoard object to game
		*/
        static void OpenBoard(void);

		/**
		* Change widget color and call ButtonPress when MouseDown
		*/
        void HandleWidgetEvent(Widget *widget, const WidgetEventData *eventData) ;
        
		/**
		* Calls GameBoard::PreprocessWidget to handle widget preprocessing
		*/
        void Preprocess(void);
		/**
		* Calls Board::MoveWidget to handle widget movement
		*/
        void MoveWidget(void);
        
		/**
		* Invalidate widget for updating
		*/
        void UpdateDisplayPosition(void);

        
		/**
		* Handling button press.
		* Gets world instance and local player instance key to handle start and pause widgets
		*/
		 void ButtonPress();

		 /**
		 * Update local player health by passing an integer with the newCurrentHealth
		 */
     void UpdateHealthBar(int newCurrentHealth);
	 /**
	 * Handles score updates when local player hits target successfully.
	 * Prevents from score reaching negative.
	 * Passes int32 as the new score to the myScore text widget
	 */
	 void UpdateScore(int32 newScore);

	 /**
	 * Updates the respawn timer with the remaining time in milliseconds passed as a variable
	 * and sets the text widget for death counter with the new time left text
	 */
	 void UpdateDeathTimer(int32 remainingTimeMilliseconds);

	 /**
	 * If death timer is finished, reset local player death counter widget to empty text
	 */
	 void ClearDeathTimer();
	 /**
	 * Handles win condition message
	 * Displays win message if local player won match
	 * Display lose message if local player lost match
	 */
	 void UpdateEndingMessage(bool Winner);
	 /**
	 * Handles display of pause meny widget
	 */
	 void ShowPauseMenu();
     
    };

	

    extern DisplayBoard *TheDisplayBoard;

	/**
	* Encapsulates components of the start window
	* Initializes the start window that contains button widgets for hosting and joining a game
	*/
       class StartWindow : public GameWindow, public Completable<StartWindow>
        {
                protected:

                        const char                              *stringTableName;

                        TextButtonWidget                *hostButton;
						TextButtonWidget                *joinButton;
						EditTextWidget	                *addressText;
                        TextButtonWidget                *cancelButton;



                        void UpdatePreview(void);

                public:
						StartWindow(const char *panelName);

                        ~StartWindow();
						/**
						* Preprocess widgets, sets host, join, and address widgets by finding the respective widgets by name reference
						*/
                        void PreprocessWidget(void) override;

						/**
						* Handles loading a hosted world or joining an existing game by providing address via the addressText widget
						*/
                        void HandleWidgetEvent(Widget *widget, const WidgetEventData *eventData) override;
        };
};

#endif
